#!/usr/bin/env python3

import argparse
import codecs
from functools import partial
import socket


def create_response(command: str, text: str):
    func_for_command = {
        "UPPER": str.upper,
        "LOWER": str.lower,
        "CAMEL": str.title,
        "SWAP": str.swapcase,
        "ROT13": partial(codecs.encode, encoding="rot13"),
    }
    try:
        response = func_for_command[command](text)
    except KeyError:
        response = f"Warning: command {command} not implemented"

    return response


def client(host: str, port: int):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((host, port))
        while True:
            command = input("Enter command: ")
            text = input("Enter text: ")

            sock.sendall(f"{command}|:|{text}".encode())
            payload = sock.recv(4096)
            print(payload.decode())


def server(host: str, port: int):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.bind((host, port))
        sock.listen(1)
        conn, client = sock.accept()
        with conn:
            while True:
                payload = conn.recv(4096)

                if not payload:
                    break

                command, text = payload.decode().split("|:|")
                response = create_response(command, text)

                conn.sendall(response.encode())


parser = argparse.ArgumentParser(
    description="Client and server for TCP echo",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "--endpoint",
    type=str,
    default="localhost:12110",
    help="Endpoint on which the server runs or to which the client connects",
)
subparsers = parser.add_subparsers(dest="{client, server}")
subparsers.required = True
parser_client = subparsers.add_parser("client")
parser_client.set_defaults(func=client)
parser_server = subparsers.add_parser("server")
parser_server.set_defaults(func=server)

args = parser.parse_args()
host = args.endpoint.split(":")[0]
port = int(args.endpoint.split(":")[1])

args.func(host, port)
