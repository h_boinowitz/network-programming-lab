# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import text_pb2 as text__pb2


class TextManipulationServiceStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.Upper = channel.unary_unary(
                '/proto.TextManipulationService/Upper',
                request_serializer=text__pb2.TextRequest.SerializeToString,
                response_deserializer=text__pb2.TextResponse.FromString,
                )
        self.Lower = channel.unary_unary(
                '/proto.TextManipulationService/Lower',
                request_serializer=text__pb2.TextRequest.SerializeToString,
                response_deserializer=text__pb2.TextResponse.FromString,
                )
        self.Rot13 = channel.unary_unary(
                '/proto.TextManipulationService/Rot13',
                request_serializer=text__pb2.TextRequest.SerializeToString,
                response_deserializer=text__pb2.TextResponse.FromString,
                )
        self.Swap = channel.unary_unary(
                '/proto.TextManipulationService/Swap',
                request_serializer=text__pb2.TextRequest.SerializeToString,
                response_deserializer=text__pb2.TextResponse.FromString,
                )


class TextManipulationServiceServicer(object):
    """Missing associated documentation comment in .proto file."""

    def Upper(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def Lower(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def Rot13(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def Swap(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_TextManipulationServiceServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'Upper': grpc.unary_unary_rpc_method_handler(
                    servicer.Upper,
                    request_deserializer=text__pb2.TextRequest.FromString,
                    response_serializer=text__pb2.TextResponse.SerializeToString,
            ),
            'Lower': grpc.unary_unary_rpc_method_handler(
                    servicer.Lower,
                    request_deserializer=text__pb2.TextRequest.FromString,
                    response_serializer=text__pb2.TextResponse.SerializeToString,
            ),
            'Rot13': grpc.unary_unary_rpc_method_handler(
                    servicer.Rot13,
                    request_deserializer=text__pb2.TextRequest.FromString,
                    response_serializer=text__pb2.TextResponse.SerializeToString,
            ),
            'Swap': grpc.unary_unary_rpc_method_handler(
                    servicer.Swap,
                    request_deserializer=text__pb2.TextRequest.FromString,
                    response_serializer=text__pb2.TextResponse.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'proto.TextManipulationService', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class TextManipulationService(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def Upper(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/proto.TextManipulationService/Upper',
            text__pb2.TextRequest.SerializeToString,
            text__pb2.TextResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def Lower(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/proto.TextManipulationService/Lower',
            text__pb2.TextRequest.SerializeToString,
            text__pb2.TextResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def Rot13(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/proto.TextManipulationService/Rot13',
            text__pb2.TextRequest.SerializeToString,
            text__pb2.TextResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def Swap(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/proto.TextManipulationService/Swap',
            text__pb2.TextRequest.SerializeToString,
            text__pb2.TextResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
