#!/usr/bin/env python3

import codecs
from concurrent import futures
import grpc
import text_pb2
import text_pb2_grpc
from functools import partial


class TextManipulation(text_pb2_grpc.TextManipulationService):
    def __init__(self) -> None:
        super().__init__()

    def Upper(self, request, context):
        return manipulate(request, str.upper)

    def Lower(self, request, context):
        return manipulate(request, str.lower)

    def Swap(self, request, context):
        return manipulate(request, str.swapcase)

    def Rot13(self, request, context):
        return manipulate(request, partial(codecs.encode, encoding="rot13"))


def manipulate(request: text_pb2.TextRequest, func_: callable) -> text_pb2.TextResponse:
    return text_pb2.TextResponse(text=func_(request.text))


server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
text_pb2_grpc.add_TextManipulationServiceServicer_to_server(TextManipulation(), server)
server.add_insecure_port("[::]:50051")
server.start()
server.wait_for_termination()
