# gRPC Text-Manipulation
The same text manipulation methods as in the `tcp-echo` and `udp-echo` are implemented using the `grpc`-library.

## Supported Text-Manipulation Functions
- `upper`: All characters to upper case
- `lower`: All characters to upper case
- `swap`: Switch case of all characters
- `rot13`: Encode given string with Caesar-cipher

## Usage
1. Start Server by running
    ```zsh
    ./server.py
    ```
    If that fails, you have to make the file executable first by running
    `chmod +x server.py`
2. Run commands in a second terminal by
   ```zsh
   ./client {command} {text}
   ```

The commands are defined as above.