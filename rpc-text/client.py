#!/usr/bin/env python3

import argparse
import grpc
import text_pb2
import text_pb2_grpc


parser = argparse.ArgumentParser()
parser.add_argument("command", type=str)
parser.add_argument("text", type=str)
args = parser.parse_args()
command, text = args.command, args.text


def run():
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = text_pb2_grpc.TextManipulationServiceStub(channel)
        func_for_command = {
            "lower": stub.Lower,
            "upper": stub.Upper,
            "rot13": stub.Rot13,
            "swap": stub.Swap,
        }

        try:
            func_ = func_for_command[command]
            resp = func_(text_pb2.TextRequest(text=text))
            print(resp.text)
        except KeyError as err:
            print("Unknown Command")


if __name__ == "__main__":
    run()
