# Network Programming Lab

This lab was part of the Computer Science RampUp course @ TU Braunschweig in the 
Summer Semester 2022.

The original assigment was

> The goal of this lab assignment is to get you started with network programming
in python. \
\
This lab assignment consist of three parts. In `udp-echo/` you are expected
to implement a simple UDP echo server that is able to respond to different
commands specified as text.  
In `tcp-echo/` you will replace the UDP communication in the echo server
with TCP.  
In `rpc-kv/` you will implement a key-value storage using RPC.

We have also implemented the same behavior as in the `tcp-echo` and `udp-echo` projects in the `grcp` library as well. The implementation can be found in `rcp-text`.