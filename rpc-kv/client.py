#!/usr/bin/env python3

import argparse
import grpc
import kv_pb2
import kv_pb2_grpc


parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="command")
subparsers.required = True
parser_insert = subparsers.add_parser("insert")
parser_insert.add_argument("key", type=str)
parser_insert.add_argument("value", type=str)
parser_lookup = subparsers.add_parser("lookup")
parser_lookup.add_argument("key", type=str)
parser_keys = subparsers.add_parser("keys")

args = parser.parse_args()


def insert(stub: kv_pb2_grpc.KeyValueServiceStub, key: str, value: str):
    return stub.Insert(kv_pb2.InsertRequest(key=key, value=value))


def lookup(stub: kv_pb2_grpc.KeyValueServiceStub, key: str):
    return stub.Lookup(kv_pb2.LookupRequest(key=key))


def keys(stub: kv_pb2_grpc.KeyValueServiceStub):
    return stub.Keys(kv_pb2.KeysRequest())


def run():
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = kv_pb2_grpc.KeyValueServiceStub(channel)
        if args.command == "insert":
            resp = insert(stub, args.key, args.value)
            if resp.success:
                print("Inserted successfully")
            else:
                print("Key already in use")
        elif args.command == "lookup":
            resp = lookup(stub, args.key)
            if resp.value:
                print(resp.value)
            else:
                print(f"Key {args.key} not found")
        elif args.command == "keys":
            resp = keys(stub)
            print(resp.keys)


if __name__ == "__main__":
    run()
